package servlet;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

public class DVDServlet extends HttpServlet {
    /*private ConnectionFactory factory;
    private Connection connection;
    private Channel channel;*/
    //private final static String QUEUE_NAME = "hello";
    private static final String EXCHANGE_NAME = "logs";
    @Override
    public void init() throws ServletException {
        /*try {
            factory = new ConnectionFactory();
            factory.setHost("localhost");
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
            //channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }*/

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n" +
                "<html>\n" +
                "   <body>\n" +
                "         <br>" +
                "      <form action = \"/dvd\" method = \"POST\">\n" +
                "         Title: <input type = \"text\" name = \"title\">\n" +
                "         <br />\n" +
                "         Year: <input type = \"text\" name = \"year\" />\n" +
                "         <br>" +
                "         Price: <input type = \"text\" name = \"price\" />\n" +
                "         <br>" +
                "         <input type=\"submit\" name=\"action\" value=\"AddNewDVD\">\n" +
                "      </form>\n" +
                "   </body>\n" +
                "</html>";
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println(page);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String action=request.getParameter("action");
        switch (action) {
            case "AddNewDVD":
                try {
                    addNewDVD(request, response);
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    private void addNewDVD(HttpServletRequest request, HttpServletResponse response) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        String title=request.getParameter("title");
        int year= Integer.parseInt(request.getParameter("year"));
        double price=Double.parseDouble(request.getParameter("price"));

        System.out.println(channel);

        servlet.DVD dvdToSend=new servlet.DVD(title,year,price);

        byte[] dvdSent=dvdToSend.getBytes();
        //channel.basicPublish("", QUEUE_NAME, null, dvdSent);
        channel.basicPublish(EXCHANGE_NAME, "", null, dvdSent);
        System.out.println(" [x] Sent '" +dvdToSend.toString() + "'");

//        channel.close();
//        connection.close();
        response.sendRedirect("/dvd");
    }
}
