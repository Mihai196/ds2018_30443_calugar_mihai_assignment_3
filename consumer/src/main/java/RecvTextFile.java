import com.rabbitmq.client.*;
import servlet.DVD;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class RecvTextFile {

    private final static String QUEUE_NAME = "hello";
    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws Exception {


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        System.out.println(channel);

        //channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(queueName);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            int messageIndex=0;
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                messageIndex++;
                DVD receivedDVD=DVD.fromBytes(body);
                System.out.println(receivedDVD.toString());
                FileWriter fileWriter = new FileWriter("DVDFile"+messageIndex+".txt");
                PrintWriter printWriter = new PrintWriter(fileWriter);
                printWriter.print(receivedDVD.toString());
                printWriter.close();

                System.out.println(" [x] Received '" + receivedDVD.toString() + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}